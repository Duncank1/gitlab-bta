const gitlabAPI = require("../api.js");
const mustache = require("mustache");

module.exports = {
    execute: function (resourceType, resource, comment) {
        return gitlabAPI.post(resourceType + "/" + resource.iid + "/discussions", {
            body: mustache.render(comment, resource),
        });
    },
    dry: function (resourceType, resource, comment) {
        let commentCompiled = mustache.render(comment, resource);
        console.debug(`Would add a discussion (or thread) ${commentCompiled} to the ${resourceType} ${resource.iid}`);
    },
};
