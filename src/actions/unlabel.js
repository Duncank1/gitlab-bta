const gitlabAPI = require("../api.js");

module.exports = {
    execute: function (resourceType, ressource, label) {
        return gitlabAPI.post(resourceType + "/" + ressource.iid + "/notes", {
            body: `/unlabel ~"${label}"`,
        });
    },
    dry: function (resourceType, resource, label) {
        console.debug(`Would remove the label ${label} to the ${resourceType} ${resource.iid}`);
    },
};
