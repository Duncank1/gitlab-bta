const gitlabAPI = require("../api.js");

module.exports = {
    get: async function (resourceType, conditions) {
        const response = await gitlabAPI.get(resourceType, conditions);
        return response.data;
    },
    getComplete: async function (resourceType, conditions) {
        const allResources = await this.get(resourceType, conditions);
        const allCompleteResources = allResources.map(function (oneResource) {
            return gitlabAPI.get(resourceType + "/" + oneResource.iid, conditions).then(function (response) {
                return response.data;
            });
        });
        const resources = Promise.all(allCompleteResources);
        return resources;
    },
};
