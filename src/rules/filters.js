
module.exports = {
    apply: (resources, filters) => {
        let filteredResources = resources;
        if (filters) {
            filters.forEach(function (filter) {
                console.info(`\t\tApplying filter: "${filter.name}"`);
                filteredResources = filteredResources.filter(function (resource) {
                    return filter.filter(resource);
                });
            });
            console.info(`\t\t\tFound ${filteredResources.length} resources after filtering`);
        }
        return filteredResources;
    },
};
