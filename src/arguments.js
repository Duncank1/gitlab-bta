const yargs = require("yargs");

const argv = yargs
    .command("./cli.js", "Make some automatic triage on issues and MR")
    .option("dry-run", {
        alias: "n",
        description: "Don't actually update anything, just print",
        type: "boolean",
        default: false,
    })
    .option("host-url", {
        alias: "H",
        description: "A valid host url",
        type: "string",
        default: "https://gitlab.com",
    })
    .option("token", {
        alias: "t",
        description: "A valid API token",
        type: "string",
    })
    .option("source-id", {
        alias: "s",
        description: "GitLab project ID",
        type: "string",
    })
    .option("timeout", {
        alias: "T",
        description: "API call timeout (ms)",
        type: "number",
        default: 10000,
    })
    .option("policies-file", {
        alias: "f",
        description: "A valid policies JS file",
        type: "string",
        default: "../policies.js",
    })
    .demandOption(["token", "source-id"], "Please provide token run and source-id arguments")
    .help()
    .alias("help", "h")
    .argv;

module.exports = {
    args: argv,
};
