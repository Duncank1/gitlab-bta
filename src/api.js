const axios = require("axios");

let PROJECT_ID;
let API_TOKEN;
let HOST_URL;
let TIMEOUT;
let BASE_URL;

let instance;

module.exports = {
    init: function (params) {
        PROJECT_ID = params["source-id"];
        API_TOKEN = params["token"];
        HOST_URL = params["host-url"];
        TIMEOUT = params["timeout"];
        BASE_URL = `${HOST_URL}/api/v4/projects/${PROJECT_ID}/`;

        instance = axios.create({
            baseURL: BASE_URL,
            timeout: TIMEOUT,
            headers: {
                "PRIVATE-TOKEN": API_TOKEN,
            },
        });
    },
    get: function (url, params) {
        return instance.get(url, {
            params: params,
        });
    },
    post: function (url, data) {
        return instance.post(url, data);
    },
    put: function (url, data) {
        return instance.put(url, data);
    },
};
