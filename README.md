[![pipeline status](https://gitlab.com/MuyBien/gitlab-bta/badges/master/pipeline.svg)](https://gitlab.com/MuyBien/gitlab-bta/-/commits/master)
[![coverage report](https://gitlab.com/MuyBien/gitlab-bta/badges/master/coverage.svg)](https://gitlab.com/MuyBien/gitlab-bta/-/commits/master)
[![npm version](https://badge.fury.io/js/gitlab-bta.svg)](https://badge.fury.io/js/gitlab-bta)

Inspired from the [GitLab Triage Bot](https://gitlab.com/gitlab-org/gitlab-triage), this bot only uses the GitLab API. So there is some functionality more difficult to do but other more simple.  
> If it is in the API you can use it.  

## Installation
To install it, just run

```
npm install -g gitlab-bta
```

## Use
```
# gitlab-bta --help
gitlab-bta [command]

Commands:
  gitlab-bta ./cli.js  Make some automatic triage on issues and MR

Options:
  --version            Show version number                             [boolean]
  --dry-run, -n        Don't actually update anything, just print
                                                      [boolean] [default: false]
  --host-url, -H       A valid host url[string] [default: "https://gitlab.com/"]
  --token, -t          A valid API token                     [string] [required]
  --source-id, -s      GitLab project ID                     [string] [required]
  --timeout, -T        API call timeout (ms)           [number] [default: 10000]
  --policies-file, -f  A valid policies JS file
                                            [string] [default: "../policies.js"]
  --help, -h           Show help                                       [boolean]
```

In your gitlab-ci.yml file you can add jobs like this:

```yml
stages:
  - triage

.triage:
  image: node:8
  stage: triage
  before_script:
    - npm install -g gitlab-bta
    - gitlab-bta --version
    - gitlab-bta --help

dry-run:
  extends: .triage
  script:
    - gitlab-bta --dry-run --host-url="http://gitlab.subdomain.fr" --token $API_TOKEN --source-id="1" --policies-file ./triage-policies.js
  when: manual

run:
  extends: .triage
  script:
    - gitlab-bta --host-url="http://gitlab.subdomain.fr" --token $API_TOKEN --source-id="1" --policies-file ./triage-policies.js
  only:
    - schedules
```

## Policies file  
Each rule is defined in a policies.js file. Written in JS, it allows you to make some computed properties.  

```js
module.exports = {
    resource_rules: {
        merge_requests: {
            rules: [{
                name: "No Bug label",
                conditions: {
                    state: "opened",
                },
                filters: [{
                    name: "No Bug label",
                    filter: function (resource) {
                        return !resource.labels.includes("Bug");
                    },
                }],
                actions: [{
                    name: "label",
                    value: "Status: to complete",
                },{
                    name: "comment",
                    value:  "Hey @{{author.username}}, there is a problem here!",
                }],
            }],
        },
    },
};
```

## Name  
You can define a name for each rule making it more obvious what the rule does.

## Conditions  
Conditions are the parameters used to search in the GitLab API:
* for [merge requests](https://docs.gitlab.com/ee/api/merge_requests.html#list-merge-requests)
* for [issues](https://docs.gitlab.com/ee/api/issues.html#list-issues)

```js
conditions: {
    state: "opened", // will get only opened resources
    labels: "none", // without any label
},
```

### Additional information
If you need to use information that is only returned with the resource detail endpoint (`/api/v4/projects/1/issues/1` for example), add `additional_info: true` to the rule.  
When this parameter is present, the GitLab BTA will get all the resources by calling the detail endpoint for each resource found with the passed conditions.  
Then you will be able to use some additional information like `diverged_commits_count` in the filter and actions parts.  
Be aware that the rule treatment will take more longer as it call the API for each founded resources.

```js
name: "Too diverged",
additional_info: true,
conditions: {
    state: "opened",
    wip: "no",
    include_diverged_commits_count: true, // this parameter will only be used for the detail API call
},
filters: [{
    name: "Too diverged",
    filter: function (resource) {
        return resource.diverged_commits_count < 50;
    },
}],
actions: [{
    name: "thread",
    value: `{{source_branch}} is too far from {{target_branch}} ({{diverged_commits_count}} commits).`,
}
```

## Filters  
You can add somme additional filters, impossible to do with the API. Those are functions taking a resource as input and returning true to keep it or false to filter it.

```js
filters: [{
    name: "Already pointed",
    filter: function (resource) {
        return !resource.labels.includes("Status: Stale");
    },
}],
```

## Actions  
In the actions part, you can define some actions to do.

### Label
Add a label to the resource.
```js
actions: [{
    name: "label",
    value: "Status: Stale",
}],
```

### Unlabel
Remove a label from a resource.
```js
actions: [{
    name: "unlabel",
    value: "Status: Stale",
}],
```

### Comment
Add a comment to a resource. You can use the quick actions (like `/cc`). You can use the resource data in the comment with mustaches.
```js
actions: [{
    name: "comment",
    value: `Hey @{{author.username}}, there is a problem here!`,
}],
```

### Thread
Add a thread (or discussion) to a resource. Adding a thread to a MR can block it if you configured your project like this. You can use the resource data in the thread with mustaches.
```js
actions: [{
    name: "thread",
    value: `Hey @{{author.username}}, there is a problem here!`,
}],
```

### Update
Update a resource. You can update all the data available in the PUT endpoint for that resource ([doc for issues](https://docs.gitlab.com/ee/api/issues.html#edit-issue) and [doc for MRs](https://docs.gitlab.com/ee/api/merge_requests.html#update-mr)).
```js
actions: [{
    name: "update",
    value: {
        title: "New title",
    },
}],
```

### Close
Close a resource
```js
actions: [{
    name: "close",
}],
```
