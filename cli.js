#!/usr/bin/env node

const commandArgs = require("./src/arguments").args;
const gitlabAPI = require("./src/api.js");

const resources = require("./src/rules/resources.js");
const filters = require("./src/rules/filters.js");
const actions = require("./src/rules/actions.js");

// Load policies
const policies = require(commandArgs["policies-file"])(commandArgs);

const ruleProcess = function (resourceType, rule) {
    console.info(`\tProcessing rule : "${rule.name}"`);
    let resourcesData = rule.additional_info ? resources.getComplete(resourceType, rule.conditions) : resources.get(resourceType, rule.conditions);
    resourcesData.then(function (resourceData) {
        let resourcesFiltered = filters.apply(resourceData, rule.filters);
        resourcesFiltered.forEach(function (mergeRequest) {
            actions.execute(resourceType, mergeRequest, rule.actions, commandArgs["dry-run"]);
        });
    }).catch(function (error) {
        console.error(`\x1b[41mError while processing rule ${rule.name}: ${error}\x1b[0m`);
        process.exitCode = 1;
    });
};

gitlabAPI.init(commandArgs);

// ForEach MR
console.info("Processing rules for merge_requests");
if (typeof policies.resource_rules.merge_requests !== "undefined") {
    policies.resource_rules.merge_requests.rules.forEach(function (rule) {
        ruleProcess("merge_requests", rule);
    });
}

// ForEach Issue
console.info("Processing rules for issues");
if (typeof policies.resource_rules.issues !== "undefined") {
    policies.resource_rules.issues.rules.forEach(function (rule) {
        ruleProcess("issues", rule);
    });
}
